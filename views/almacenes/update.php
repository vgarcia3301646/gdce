<div class="almacenes-update">
    <!-- Renderiza un formulario de actualización -->
    <?= $this->render('_updateForm', [
        'model' => $model,
    ]) ?>
</div>
